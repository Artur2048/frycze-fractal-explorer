CC=gcc
CFLAGS= -Wall -O3 $(shell pkg-config --cflags gtk+-3.0)
LDFLAGS= -lm -lpthread $(shell pkg-config --libs gtk+-3.0)

mandelbrot: ffe.c
	$(CC) $(CFLAGS) -o ffe ffe.c $(LDFLAGS)

all: mandelbrot

clean:
	rm -f ffe

