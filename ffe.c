/*
Frycze Fractal Explorer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Author: Artur Frycze

	https://www.linkedin.com/in/artur-frycze/

	...to my Father - Roman Sylwester Frycze

*/

#define APP_VERSION "1.0"

#include <gtk/gtk.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

#include "ffe_icon.h"

// initial configuration
double Xc = -1.416367856816; double Yc = -0.000000395968; double Zoom = 0.68; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 390;
//double Xc = -1.446737206416; double Yc = 0.000600791001; double Zoom = 0.65; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 620;
//double Xc = -0.75; double Yc = 0.0; double Zoom = 0.75; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 220;
//double Xc = -0.7425; double Yc = -0.1638; double Zoom = 2.5e+02; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 170;

//double Xc = -0.7425; double Yc = -0.1638; double Zoom = 5e+03; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 470; // for testing purposes - avg rendering time = 145ms
//double Xc = -0.739451; double Yc = -0.164812; double Zoom = 1.470417e+02; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 470;
//double Xc = -0.728895; double Yc = -0.170986; double Zoom = 1.196960e+03; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 470;
//double Xc = -0.728955; double Yc = -0.171177; double Zoom = 4.924865e+04; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 470;
//double Xc = -0.742083786; double Yc = -0.163527047; double Zoom = 3.363750e+04; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 470;

//double Xc = -0.740295285; double Yc = -0.183479773; double Zoom = 3.948734e+06; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 10000;

// FAST TESTING
//double Xc = -0.75; double Yc = 0.0; double Zoom = 0.5; int CANVAS_X = 1200; int CANVAS_Y = 820; int Imax = 10;
//double Xc = -0.7425; double Yc = -0.1638; double Zoom = 2.5e+02; int CANVAS_X = 1400; int CANVAS_Y = 920; int Imax = 30;

// copy of initial configuration - used in the fractal reset()
double Xc_start, Yc_start, Zoom_start;
int Imax_start, Color_mode_start;

double Xmin, Xmax, Ymin, Ymax;	// real screen coordinates (X = Cre, Y = Cim)
double C_step;	// increment step of C for one pixel-x -0.742499997700 -y -0.163799728053 -z 3.948735e+06 -i 960


GtkWidget *window, *box, *layout, *image, *event_box, *statusbar;
GdkPixbuf *pixbuf;
guchar *pixels, *palette = NULL;
int rowstride;	// amount of pixels in one row
double render_time = 0;
// probably i should remove rowstride and *pxiels and get it using pixbuf methods where needed
// -- performance tests needed

struct worker_data { int Y_from, Y_to; };	// data passed to fractal_slave - line numbers to process

void render_fractal();
void calculate_screen_range();

guchar* create_palette_sine_simple(int nColors); // to be removed or replaced by macro
void create_palette_gray(int nColors);
void create_palette_sine(int nColors, int period, double fR, double fG, double fB, double dR, double dG, double dB);
void create_palette_hue(int nColors, int period, double shift, double shade0, double shade1);


int Color_mode = 1;
// TODO: color parameter (like cycle, shift, etc...): int Color_mode_param = 0;


// helper functions
void calculate_time_diff(int startstop);
void print_parameters();
void print_palette();
void info_popup();

// casting REAL <--> SCREEN
int x_real_to_screen(double x) { return (x-Xmin) * CANVAS_X / (Xmax - Xmin); }
int y_real_to_screen(double y) { return (y-Ymin) * CANVAS_Y / (Ymax - Ymin); }
double x_screen_to_real(int x) { return Xmin + (Xmax - Xmin) * (double) x / CANVAS_X; }
double y_screen_to_real(int y) { return Ymin + (Ymax - Ymin) * (double) y / CANVAS_Y; }

// TODO - implement different fractal rendering methods
// void render_fractal_singlethread();

gboolean startFullscreen = FALSE;

// command line options
static GOptionEntry cmd_params[] =
{
	{ "coordx", 'x', 0, G_OPTION_ARG_DOUBLE, &Xc, "X coordinate", "x"},
	{ "coordy", 'y', 0, G_OPTION_ARG_DOUBLE, &Yc, "Y coordinate", "y"},
	{ "zoom", 'z', 0, G_OPTION_ARG_DOUBLE, &Zoom, "Zoom level", "z"},
	{ "iterations", 'i', 0, G_OPTION_ARG_INT, &Imax, "Maximum iterations limit", "n"},
	{ "clolor", 'c', 0, G_OPTION_ARG_INT, &Color_mode, "Color mode", "c"},
	{ "full-screen", 'f', 0, G_OPTION_ARG_NONE, &startFullscreen, "Fullscreen", NULL}
};


// new function to build color palettes. replaces old macros "create_palette"
void colorize()
{
	switch(Color_mode)
	{
		case 1:  // Hue cycle - period 100, 5% shades on each end
			create_palette_hue(Imax+1, 100, 0, 0.05, 0.95);
			break;

		case 2:  // periodic sine waves with phase shift
			create_palette_sine(Imax+1, 128,   2.0, 2.0, 2.0,   0.5, 0.5 + 2.0/3.0, 0.5 + 4.0/3.0);
			break;

		case 3:  // different frequencies sine waves - shade on both ends (first implementation: sine_simple)
			create_palette_sine(Imax+1, Imax+1,   2, 4, 6,   0, 0, 0);
			break;

		case 4:  // grayscale
			create_palette_gray(Imax+1);
			break;

		case 5:  // experimental - to be changed
			create_palette_hue(Imax+1, 200, 0, 0.1, 0.9);
			break;
	}

}

/******************************************************************************
*  CALLBACKS
******************************************************************************/
static gboolean scroll_callback(GtkWidget *widget, GdkEventScroll *event)
{
	static guint t_id = 0;

	gboolean zoom_fractal (gpointer user_data)
	{
		guint *t_id_ptr = user_data;
		*t_id_ptr = 0;

//		printf("Zoom: execute [%e]\n", Zoom);

		calculate_screen_range();
		render_fractal();
		gtk_image_set_from_pixbuf (GTK_IMAGE(image), pixbuf);

		return FALSE;
	}

	if(event->direction) Zoom /= 1.1;
	else Zoom *= 1.1;

//	printf("Zoom: %c [%e]\n", event->direction?'-':'+', Zoom);

	if(t_id) g_source_remove(t_id);
	t_id = g_timeout_add(25, zoom_fractal, &t_id);

	return TRUE;
}


static gboolean click_callback (GtkWidget *widget, GdkEventButton *event)
{
//	printf("B: %d   X: %3d   Y: %3d\n", event->button, (int)event->x, (int)event->y);

	if(event->button == 1 || event->button == 2 || event->button == 3)
	{
		Xc = x_screen_to_real((int)event->x);
		Yc = y_screen_to_real((int)event->y);
		calculate_screen_range();
		render_fractal();
	}

	if(event->button == 8)
	{
		Imax += (Imax < 10)?1:(Imax<100)?5:10;
		colorize();
		render_fractal();
	}
	if(event->button == 9)
	{
		Imax -= (Imax <= 10)?1:(Imax<=100)?5:10;
		if(Imax < 1) Imax = 1;
		colorize();
		render_fractal();
	}

	gtk_image_set_from_pixbuf (GTK_IMAGE(image), pixbuf);  // isn't it SLOW like hell?

	return TRUE;
}

// TO CHECK: should i implement CONFIGURE signal instead of size_allocate ??? - tests needed
static gboolean size_allocate_callback(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data)
{
	static int current_width = 0, current_height = 0;
	static guint t_id = 0;

	gboolean resize_window (gpointer user_data)
	{
		guint *t_id = user_data;
		*t_id = 0;

//		printf("resize_window *EXECUTE*:   set to: %dx%d\n", current_width, current_height);
		CANVAS_X = current_width;
		CANVAS_Y = current_height;

		calculate_screen_range();

		g_object_unref(pixbuf);	// should we save it and unref at the end of the procedure ???
		pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, 0, 8, CANVAS_X, CANVAS_Y);
		pixels = gdk_pixbuf_get_pixels(pixbuf);
		rowstride = gdk_pixbuf_get_rowstride(pixbuf);

		render_fractal();

		gtk_image_set_from_pixbuf (GTK_IMAGE(image), pixbuf);
		return FALSE;
	}

// TRY to detect if it wasn't maximixe/unmaximize OR fullscreen/unfullscreen request (or program startup?)- then just CALL resize immediatelly (??)
// also - check what are the other reasons of getting this signal (on focus) - check data passed !!!
// GdkWindowState state = gdk_window_get_state(gtk_widget_get_window(widget));
// check if fullscreen / maximized ??? --> if(state & GDK_WINDOW_STATE_FULLSCREEN)... (add maximized)
// or maybe use is_gtk_window_is_fullscreen () / gtk_window_is_maximized ()
// if so - skip timer - call resize immediatelly.

//	printf("size_allocate_callback:  t_id=%d   allocation: %d x %d   current: %d x %d   CANVAS: %d x %d\n",
//		t_id, allocation->width, allocation->height, current_width, current_height, CANVAS_X, CANVAS_Y);

	if(allocation->width != CANVAS_X || allocation->height != CANVAS_Y) // When STATUSBAR is added - need to remodel this
//	if(allocation->width != current_width || allocation->height != current_height)
	{
//		printf("size_allocate_signal - set timer...\n");
		if(t_id) g_source_remove(t_id);
		t_id = g_timeout_add(50, resize_window, &t_id);

		current_width = allocation->width;
		current_height = allocation->height;
	}

	return FALSE;
}


gboolean save_fractal()
{
	GError *error = NULL;
	char filename[150], date_string[15];
	time_t temp;
	gboolean result;

	temp = time(NULL);
	strftime(date_string, sizeof(date_string), "%F", localtime(&temp));

	snprintf(filename, sizeof(filename), "ffe-%s_x%.8f_y%.8f_z%.3e_i%d.png", date_string, Xc, Yc, Zoom, Imax);
	printf("Saving fractal to file: %s\n", filename);
	result = gdk_pixbuf_save(pixbuf, filename, "png", &error, NULL);
	if(!result) fprintf(stderr, "Error saving fractal: %s\n", error->message);
	g_clear_error(&error);
	return result;
}


void copy_to_clipboard()
{
	gtk_clipboard_set_image(gtk_clipboard_get_default(gdk_display_get_default()), pixbuf);
}


static gboolean key_press_callback (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	GdkWindowState state = gdk_window_get_state(gtk_widget_get_window(widget));
	int increment = 10;
	int refresh = 0;

//	printf("Keypress %s state=%x [keyval=%d] [hardware_keycode=%d]\n", gdk_keyval_name(event->keyval), event->state, event->keyval, event->hardware_keycode);

	if(event->state & GDK_SHIFT_MASK) increment = 1;
	else if(event->state & GDK_CONTROL_MASK) increment = 40;

	switch(event->keyval)
	{
		case GDK_KEY_F11:
		case GDK_KEY_f:
			if(state & GDK_WINDOW_STATE_FULLSCREEN) gtk_window_unfullscreen(GTK_WINDOW(window));
			else gtk_window_fullscreen(GTK_WINDOW(window));
			break;
		case GDK_KEY_Escape:
			if(state & GDK_WINDOW_STATE_FULLSCREEN) gtk_window_unfullscreen(GTK_WINDOW(window));
			break;
		case GDK_KEY_Up:
			Yc -= C_step * increment;
			refresh = 1;
			break;
		case GDK_KEY_Down:
			Yc += C_step * increment;
			refresh = 1;
			break;
		case GDK_KEY_Left:
			Xc -= C_step * increment;
			refresh = 1;
			break;
		case GDK_KEY_Right:
			Xc += C_step * increment;
			refresh = 1;
			break;
		case GDK_KEY_KP_Add:
		case GDK_KEY_plus:
		case GDK_KEY_equal:
			Zoom *= 1.1;
			refresh = 1;
			break;
		case GDK_KEY_KP_Subtract:
		case GDK_KEY_minus:
		case GDK_KEY_underscore:
			Zoom /= 1.1;
			refresh = 1;
			break;
		case GDK_KEY_F5:
			refresh = 1;
			break;
		case GDK_KEY_bracketright:
		case GDK_KEY_Page_Up:
			Imax += (Imax < 10)?1:(Imax<100)?5:10;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_bracketleft:
		case GDK_KEY_Page_Down:
			Imax -= (Imax <= 10)?1:(Imax<=100)?5:10;
			if(Imax < 1) Imax = 1;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_BackSpace:
		case GDK_KEY_Home:
			Xc = Xc_start;
			Yc = Yc_start;
			Zoom = Zoom_start;
			Imax = Imax_start;
			Color_mode = Color_mode_start;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_Return:
		case GDK_KEY_space:
			print_parameters();
			break;
		case GDK_KEY_q:
			gtk_widget_destroy(window);
			break;
		case GDK_KEY_s:
			if(!(event->state & GDK_CONTROL_MASK)) break;
		case GDK_KEY_F2:
			save_fractal();
			break;
		case GDK_KEY_c:
			if(event->state & GDK_CONTROL_MASK) copy_to_clipboard();
			break;
		case GDK_KEY_1:
			Color_mode = 1;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_2:
			Color_mode = 2;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_3:
			Color_mode = 3;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_4:
			Color_mode = 4;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_5:
			Color_mode = 5;
			colorize();
			refresh = 1;
			break;
		case GDK_KEY_F1:
			info_popup();
			break;
	}

	if(refresh)
	{
		calculate_screen_range();
		render_fractal();
		gtk_image_set_from_pixbuf (GTK_IMAGE(image), pixbuf);
	}

	return TRUE;
}


// AUTOTEST functions - to detect memory leaks, and performance testing
gboolean test_auto_zoom(gpointer user_data)
{
	static gboolean flipflop = FALSE;
	static int nz = 0;
	GdkEventScroll ev;
	printf("test: auto-zoom\n");

	if(nz++>15)
	{
		flipflop = !flipflop;
		nz = 0;
	}
	ev.direction = flipflop?1:0;
	scroll_callback(window, &ev);
	return TRUE;
}

gboolean test_auto_resize(gpointer user_data)
{
	static gboolean flipflop = FALSE;
	printf("test: auto-resize\n");
	if(flipflop) gtk_window_unmaximize(GTK_WINDOW(window));
	else gtk_window_maximize(GTK_WINDOW(window));
	flipflop = !flipflop;
	return TRUE;
}

////////////////////
/*   SET UP GUI   */
////////////////////
void info_popup()  // WORK IN PROGRESS...
{
	char buf[500];
	GtkWidget *dialog, *dialog_box, *label1;
	GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;

	dialog = gtk_dialog_new_with_buttons("Frycze Fractal Explorer v"APP_VERSION, GTK_WINDOW(window), flags, "_OK", GTK_RESPONSE_ACCEPT, NULL);
	dialog_box = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

	snprintf(buf, sizeof(buf), "\nFractal parameters\n----------------------------\nXY:   [ %.12f,   %.12f ]\nZoom:   %e\nnumber of iterations:   %d\n\nColor mode:   %d\nCanvas:   %d x %d\nlast frame rendering time:   %.2f ms\n",
		Xc, Yc, Zoom, Imax, Color_mode, CANVAS_X, CANVAS_Y, render_time);

	label1 = gtk_label_new(buf);

	gtk_box_pack_start(GTK_BOX(dialog_box), label1, TRUE, TRUE, 0);
	gtk_widget_show(label1);

	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}


static void activate (GtkApplication* app, gpointer user_data)
{
//	guint statusbar_id;  // TODO
	window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "Frycze Fractal Explorer v"APP_VERSION);

	gtk_widget_set_size_request (GTK_WIDGET(window), 265, 150);	// minimum size

// TODO: change Layout to Box - need to reorganize resize_callback
	layout = gtk_layout_new(NULL, NULL); // <<<<<<<<<<<<<<<<<<<< maybe chamge to VBOX
//	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0); //// VBOX: for some reason cannot resize down...

	pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, 0, 8, CANVAS_X, CANVAS_Y);
	pixels = gdk_pixbuf_get_pixels(pixbuf);
	rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	image = gtk_image_new_from_pixbuf(pixbuf);
	event_box = gtk_event_box_new();
	gtk_container_add(GTK_CONTAINER(event_box), image);


	gtk_container_add(GTK_CONTAINER(layout), event_box);
//	gtk_box_pack_start(GTK_BOX(box), layout, FALSE, FALSE, 0);

/*	statusbar = gtk_statusbar_new();
	gtk_container_add(GTK_CONTAINER(layout), statusbar);
	statusbar_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "info");
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), statusbar_id, "Hello");
*/
	gtk_container_add(GTK_CONTAINER(window), layout);  // change from layout

	gtk_window_set_default_size(GTK_WINDOW(window), CANVAS_X, CANVAS_Y);

	gtk_widget_add_events(event_box, GDK_SCROLL_MASK);
	g_signal_connect(event_box, "scroll-event", G_CALLBACK (scroll_callback), NULL);
	g_signal_connect(event_box, "button_press_event", G_CALLBACK (click_callback), NULL);
	g_signal_connect(window, "size-allocate", G_CALLBACK(size_allocate_callback), NULL);
	g_signal_connect(window, "key-press-event", G_CALLBACK(key_press_callback), NULL);

	if(startFullscreen) gtk_window_fullscreen(GTK_WINDOW(window));
	else render_fractal();   // PROBABLY we don't need it, cause we will get size_allocate_signal on first run

	GdkPixbuf *ffe_icon_pixbuf = gdk_pixbuf_new_from_data(ffe_icon, GDK_COLORSPACE_RGB, TRUE, 8, 48, 48, 192, NULL, NULL);
	gtk_window_set_icon(GTK_WINDOW(window), ffe_icon_pixbuf);

	gtk_widget_show_all (window);

//// TESTING - run auto-zoom and auto-resize - performance testing /////
//	g_timeout_add(400, test_auto_resize, NULL);
//	g_timeout_add(80, test_auto_zoom, NULL);
}


#define N_THREADS 12
void render_fractal()
{
	pthread_t thread_id[N_THREADS];
	struct worker_data w_data[N_THREADS];
	int i, lines_per_thread;

	void* fractal_slave(void *data)
	{
		struct worker_data *w_data = (struct worker_data *) data;
		int n, x, y;
		double Zre, Zim, Cre, Cim, Zre2, Zim2;

//		printf("Slave renderer processing...: %3d - %3d\n", w_data->Y_from, w_data->Y_to);

//	Cim = y_screen_to_real(w_data->Y_from); // <-- optimized using addition below:
		Cim = Ymin + w_data->Y_from * C_step;

		for(y=w_data->Y_from; y<w_data->Y_to; y++)
		{
//		Cim = y_screen_to_real(y); // optimization: replaced with precalculated value

			Cre = Xmin;
			for(x=0; x<CANVAS_X; x++)
			{
//			Cre = x_screen_to_real(x); // optimization: replaced with precalculated value
				Zre = Cre;
				Zim = Cim;

				// TODO: try to swap I loop with X loop
				// - use ARRAYS and try to help GCC use AVX-512F! (make calculations on arrays, not vars)
				// - optionally try explicit intrinsics

				// #pragma opm simd
				for(n=0; n<Imax; n++)
				{
					Zre2 = Zre*Zre;
					Zim2 = Zim*Zim;

					if(Zre2+Zim2 > 4) break;

					Zim = 2 * Zre * Zim + Cim;
					Zre = Zre2 - Zim2 + Cre;
				}
				pixels[y*rowstride + x*3    ] = palette[3*n];
				pixels[y*rowstride + x*3 + 1] = palette[3*n+1];
				pixels[y*rowstride + x*3 + 2] = palette[3*n+2];
				Cre += C_step;
			}

			Cim += C_step;
		}
		pthread_exit(NULL);
	}

	calculate_time_diff(0);  // rendering start

	lines_per_thread = (int) CANVAS_Y / N_THREADS;

	for(i=N_THREADS-1; i>=0; i--)
	{
		w_data[i].Y_from = i * lines_per_thread;
		w_data[i].Y_to = (i + 1) * lines_per_thread;
		if(i == N_THREADS-1) w_data[i].Y_to = CANVAS_Y;	// last thread has usually few more lines to process

    pthread_create (&thread_id[i], NULL, fractal_slave, (void*) (w_data+i));
//		printf("spawning slave thread [%2d]:  lines range: %3d - %3d\n", i, w_data[i].Y_from, w_data[i].Y_to);
	}

// wait for all the workers to finish calculations
	for(i=0; i<N_THREADS; i++)
	{
		pthread_join(thread_id[i], NULL);
	}

	calculate_time_diff(1);  // rendering stop
}


static void startup (GtkApplication* app, gpointer user_data)
{
	printf("Frycze Fractal Explorer v"APP_VERSION"\n");
	Xc_start = Xc;
	Yc_start = Yc;
	Zoom_start = Zoom;
	Imax_start = Imax;
	Color_mode_start = Color_mode;

	calculate_screen_range();    // reconsider if it should be here...
	colorize();
	print_parameters();
//	print_palette();
}

static void shutdown (GtkApplication* app, gpointer user_data)
{
	printf("Bye.\n");
	g_object_unref (pixbuf);
//	g_object_unref (ffe_icon_pixbuf);
}


static int handle_cli (GtkApplication* app, gpointer user_data)  // probably it can be moved to startup signal
{
	if(Imax<1)
	{
		fprintf(stderr, "Number of iterations should be greater than zero!\n");
		return 1;
	}
	return -1;
}


/******************************************************************************
*  MAIN LOOP
******************************************************************************/
int main (int argc, char **argv)
{
	GtkApplication *app;
	int status;

	app = gtk_application_new ("af.fractal", G_APPLICATION_NON_UNIQUE);
	g_application_add_main_option_entries(G_APPLICATION(app), cmd_params);

	g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	g_signal_connect (app, "handle-local-options", G_CALLBACK (handle_cli), NULL);
	g_signal_connect (app, "shutdown", G_CALLBACK (shutdown), NULL);

	status = g_application_run (G_APPLICATION (app), argc, argv);

	g_object_unref (app);
	return status;
}


/******************************************************************************
*  Helper functions
******************************************************************************/

// recalculate screen ranges (call in case Zoom level changes)
void calculate_screen_range()
{
	double aspect = (double) CANVAS_Y / (double) CANVAS_X;

	Xmin	= Xc - 1.5 / Zoom;
	Xmax	= Xc + 1.5 / Zoom;
	Ymin	= Yc - 1.5 * aspect  / Zoom;
	Ymax	= Yc + 1.5 * aspect  / Zoom;

	C_step = 3 / Zoom / CANVAS_X;
}


void print_palette()
{
	int n;
	printf("\ncolor palette\n-- n ---- R - G - B --\n");
	for(n=0; n<Imax+1; n++)
		printf("%4d:\t%3d %3d %3d\n", n, palette[3*n], palette[3*n+1], palette[3*n+2]);
}


// create palette based on HSV wheel
void create_palette_hue(int nColors, int period, double shift, double shade0, double shade1)
{
	double h, s, v, r, g, b;
	int n;
	double x;
//	static guchar *ptr = NULL;

	if(palette) free(palette);
	palette = (guchar*) malloc(nColors*3);

	period = MIN(period, nColors);

	s = 1;
	v = 1;
	for(n=0; n<nColors; n++)
	{
		x = (double) n / (double) nColors;
		if( x < shade0 )
		{
			v = 0.5 + 0.5 * cos( (1 + x / shade0) * M_PI );
//			v = x / shade0;  // linear gradient
		}
		else if( x > shade1 )
		{
			v = 0.5 + 0.5 * cos( (x - shade1) * M_PI / (1 - shade1) );
//			v = ( 1 - x ) / ( 1 - shade1 );  // linear gradient
		}
		else v = 1;

		h = (double) ( (n + (int)(shift*period)) % period ) / (double) period;
		gtk_hsv_to_rgb(h, s, v, &r, &g, &b);

		palette[3*n  ] = 255 * r;
		palette[3*n+1] = 255 * g;
		palette[3*n+2] = 255 * b;

//		printf("%4d:\th=%.3f s=%.3f v=%.3f -> r=%.3f g=%.3f b=%.3f\t[%3d %3d %3d]\n", n, h, s, v, r, g, b, ptr[3*n], ptr[3*n+1], ptr[3*n+2]);
	}

	palette[3*nColors-3] = 0; palette[3*nColors-3+1] = 0; palette[3*nColors-3+2] = 0;  // bounded inside fractal
//	ptr[0] = 0; ptr[1] = 0; ptr[2] = 0;  // outside of fractal (explode after 0 iterations)

//	return ptr;
}

// Create color palette - sine waves 2
void create_palette_sine(int nColors, int period, double fR, double fG, double fB, double dR, double dG, double dB)
{
//	static guchar *ptr = NULL;
	int n;

	if(palette) free(palette);
	palette = (guchar*) malloc(nColors*3);

	for(n=0; n<nColors; n++)
	{
		palette[3*n  ] = ( 1 - cos( n*fR*M_PI / period + dR*M_PI ) ) * 127.5;
		palette[3*n+1] = ( 1 - cos( n*fG*M_PI / period + dG*M_PI ) ) * 127.5;
		palette[3*n+2] = ( 1 - cos( n*fB*M_PI / period + dB*M_PI ) ) * 127.5;

//		printf("%4d: %3d %3d %3d\n", n, ptr[3*n], ptr[3*n+1], ptr[3*n+2]);
	}
	palette[3*nColors-3] = 0; palette[3*nColors-3+1] = 0; palette[3*nColors-3+2] = 0;
//	return ptr;
}

// OLD - simple sine waves   - NOT USED - can be replaced using create_palette_sine(Imax+1, Imax+1,   2, 4, 6,   0, 0, 0);
guchar* create_palette_sine_simple(int nColors)
{
	static guchar *ptr = NULL;
	int n;

	if(ptr) free(ptr);
	ptr = (guchar*) malloc(nColors*3);

	for(n=0; n<nColors; n++)
	{
		ptr[3*n  ] = (1-cos(n*2*M_PI/nColors))*127.5;
		ptr[3*n+1] = (1-cos(n*4*M_PI/nColors))*127.5;
		ptr[3*n+2] = (1-cos(n*6*M_PI/nColors))*127.5;
//		printf("%4d: %3d %3d %3d\n", n, ptr[3*n], ptr[3*n+1], ptr[3*n+2]);
	}
//	ptr[3*nColors-3] = 0; ptr[3*nColors-3+1] = 0; ptr[3*nColors-3+2] = 0;
	return ptr;
}


// color palette - simple gray gradient
void create_palette_gray(int nColors)
{
//	static guchar *ptr = NULL;
	int n;

	if(palette) free(palette);
	palette = (guchar*) malloc(nColors*3);

	for(n=0; n<nColors; n++)
	{
		palette[3*n  ] = n % 256;
		palette[3*n+1] = n % 256;
		palette[3*n+2] = n % 256;
//		printf("%4d: %3d %3d %3d\n", n, ptr[3*n], ptr[3*n+1], ptr[3*n+2]);
	}
	palette[3*nColors-3] = 0; palette[3*nColors-3+1] = 0; palette[3*nColors-3+2] = 0;
//	return ptr;
}


// calculate time difference in miliseconds
void calculate_time_diff(int startstop)
{
	static struct timespec t0, t1;

	if(startstop==0)
	{
		clock_gettime(CLOCK_MONOTONIC, &t0);
	}
	else
	{
		clock_gettime(CLOCK_MONOTONIC, &t1);
		render_time = 1000 * (t1.tv_sec - t0.tv_sec) + 0.000001 * (t1.tv_nsec - t0.tv_nsec);
		printf("rendering time: %.2f ms\n", render_time);
//		printf("rendering time: %.6f ms\n", render_time);
	}
}


// display current fractal and screen parameters
void print_parameters()
{
	printf("\nFractal parameters\n------------------------------\n");
//	printf("Xc: %.9f   Yc: %.9f   Zoom: %e   Imax: %d\n", Xc, Yc, Zoom, Imax);
	printf("-x %.12f -y %.12f -z %e -i %d -c %d\n", Xc, Yc, Zoom, Imax, Color_mode);
	printf("Canvas: %d x %d\n", CANVAS_X, CANVAS_Y);
//	printf("Xmin: %f   Xmax: %f\tYmin: %f   Ymax: %f\tC_step: %e\n", Xmin, Xmax, Ymin, Ymax, C_step);
	if(render_time) printf("last frame rendering time: %.2f ms\n", render_time);
	printf("------------------------------\n");
}
